/*
 ============================================================================
 Name        : dht11_driver.c
 Author      : Francisco Javier Moreno Conde
 Version     : 
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/timer.h>
#include <linux/init.h>
#include <linux/wait.h>       // waiting queue
#include <linux/sched.h>      
#include <linux/delay.h>      // udelay
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>
#include <linux/proc_fs.h>
#include <asm/io.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/mutex.h>
#include <linux/iio/iio.h>
#include <linux/iio/sysfs.h>
#include <linux/iio/events.h>
#include <linux/iio/buffer.h>
#include <linux/iio/trigger.h>

#define DHT_GPIO_DESC  "Pin_DHT11"
#define TIMER_OFFSET 4
#define PI2_TIMER_PAGE_BASE 0x3f003000

#define TIEMPOCERO 77
#define TIEMPOUNO 123
#define ERROR 22

#define MAX 42

DEFINE_SEMAPHORE(critical_section);
static DECLARE_WAIT_QUEUE_HEAD(wait_queue);

static irqreturn_t irq_handler_DHT(int irq, void *dev);	/*Manejador de la interrupción */

struct private_data{
	
	struct device *dev; /*device */
	unsigned int humedad_e; /*parte entero de humedad */
	unsigned int temperatura_e;/*parte entera de temperatura */
	unsigned int humedad_d; /*parte decimal de humedad */
	unsigned int temperatura_d; /*parte decimal de temperatura*/
	unsigned int checksum; /*valor de checksum*/
	
	unsigned int total_temp; /*total de temperatura, también para devolver lectura anterior */
	unsigned int total_hum; /*total de humedad, también para devolver lectura anterior */

	unsigned int irq_DHT; /*ID interrupción */

	unsigned int tiempos[MAX]; /* En este buffer guardamos los tiempos que corresponden a 1s o a 0s */
	unsigned int bits[40]; /*Este buffer guarda los 5 bytes de datos*/
	
	unsigned int pos; /* posicion del array de tiempos */

	u8 *timer;  /* El puntero para mapear el timer de 16 MHz */
	u64 time_start; /*Timestamp inicio lectura*/
	u64 time_end; /*Timestamp fin lectura */

	char gpio; /*Gpio para la transmisión de datos*/
	int lock; /*lock para cola de espera */

};

/* Función que transforma un byte a decimal*/
static unsigned int byte_a_decimal(unsigned int bits[]){
	unsigned int res = 0, i;
	for(i=0;i<8;i++){
		res<<=1;
		if(bits[i]) res++;	
	}
	return res;
	
}

/* Definimos los canales que serán usados, temperatura, humedad y un timestamp de tiempo de ejecución	*/
static const struct iio_chan_spec dht11_channels[] = {
		{
				.type = IIO_TEMP,  /*Los tipos se encuentran en el fichero "include/uapi/linux/iio/types.h" */
				.info_mask_separate = BIT(IIO_CHAN_INFO_PROCESSED)
		},
		{
				.type = IIO_HUMIDITYRELATIVE,
				.info_mask_separate = BIT(IIO_CHAN_INFO_PROCESSED)
		},
		{
				.type = IIO_TIMESTAMP,
				.info_mask_separate = BIT(IIO_CHAN_INFO_PROCESSED)
		},
};

/* Estructura usada para relacionar nuestro driver con un platform_driver para usar device-trees*/
static const struct of_device_id iio_dht11_ids[] = {
		{.compatible = "dht11_malaga", },  
		{/*Lista a NULL para indicar fin, un driver puede tener varios campos compatible */ }
};

/*Esta relacionada con la estructura anterior, nos permite declarar la tabla de ID como una 
 * tabla de dspositivos para la autocarga del módulo durante el arranque del sistema */
MODULE_DEVICE_TABLE(of, iio_dht11_ids);

/*Función que se ejecutará durante la descarga del módulo*/
static int DHT11_Remove(struct platform_device *pdev){
	
	/* obtenemos un puntero a la instancia device de la estructura iio_dev del subsistema del bus */
	struct iio_dev *iio = platform_get_drvdata(pdev); 
		 
	struct private_data *data = iio_priv(iio);

	printk(KERN_NOTICE "%s module, desregistering, releasing...\n", KBUILD_MODNAME);
	
	gpio_set_value(data->gpio, 0);
	gpio_free(data->gpio); /*liberamos el gpio */
	
	iounmap(data->timer);	/* Desmapeamos el timer */

	devm_iio_device_unregister(&iio->dev,iio); /*desregistramos el device iio */
    devm_iio_device_free(&iio->dev,iio); /*liberamos el device iio */
	
	printk(KERN_NOTICE "Done. Bye from %s module\n", KBUILD_MODNAME);
	
	return 0;

}


/*Función de lectura donde nos comunicamos con el sensor, tomamos los datos, los procesamos y 
 *los devolvemos dependiendo del canal donde se haya realizado la lectura */
static int read_raw(struct iio_dev *iio,struct iio_chan_spec const *channel, int *val, int *val2, long mask){
	int res,i=0;
	struct private_data *data = iio_priv(iio);
	data->time_start = ktime_get_boot_ns(); //para medir tiempo de ejecución
	printk(KERN_NOTICE "Time_start = %llu ns\n",data->time_start);
	/*Devolvemos los datos antiguos en caso de que no hayan transcurrido 2s 
	 * desde la lectura anterior */
	if(data->time_start - data->time_end < 2000000000){
		printk("LECTURA DE MENOS DE 2s");
		goto lectura_anterior;
	}
	down(&critical_section);
	data->lock=1;
	/*Comenzamos la comunicación según datasheet*/
	gpio_direction_output(data->gpio,0);
	usleep_range(19000,20000);


	if ((res=request_irq(data->irq_DHT,
                    irq_handler_DHT,
                    IRQF_TRIGGER_FALLING,
                    DHT_GPIO_DESC,
                    iio))) {
        printk(KERN_ERR "Irq Request failure: cannot register irq %d\n",data->irq_DHT);
        return res;
    }

	gpio_direction_input(data->gpio);
	wait_event_interruptible(wait_queue, !data->lock); /*Esperamos a que termine la toma de datos*/
	free_irq(data->irq_DHT, iio);/* Desmapeamos la interrupción */
/*	
    // Transformación de tiempos en valores binarios 
    printk(KERN_NOTICE "Tiempos sin procesar --> data->pos = %d",data->pos);
	for(i=0;i<=(data->pos);i++){
		printk(KERN_NOTICE "medida %d %d",i,data->tiempos[i]);
	}
*/    
    
    for(i=0;i<MAX-1;i++){
		data->tiempos[i] = data->tiempos[i+1] - data->tiempos[i];
	}
/*	
    printk(KERN_NOTICE "Tiempos procesados");
	for(i=0;i<=MAX-1;i++){
		printk(KERN_NOTICE "medida %d %d",i, data->tiempos[i]);
	}
*/	

	for(i=1;i<MAX-1;i++){
		if(data->tiempos[i] >= (TIEMPOCERO-ERROR) && data->tiempos[i] <= (TIEMPOCERO+ERROR)){
			data->bits[i-1] = 0;
		}else if (data->tiempos[i] >= (TIEMPOUNO-ERROR) && data->tiempos[i] <= (TIEMPOUNO+ERROR)){
			data->bits[i-1] = 1;
		}else{
			printk(KERN_NOTICE "Error en la transformación de tiempos a bits");
			goto lectura_anterior;
		}
	}
/*
	printk(KERN_NOTICE "Bits\n");
	for(i=0;i<40;i++){
		printk(KERN_NOTICE "bit %d %d",i,data->bits[i]);
	}
*/
	/* Realizamos la conversión de cada byte a decimal*/
	data->humedad_e=byte_a_decimal(&data->bits[0]);
    data->humedad_d=byte_a_decimal(&data->bits[8]);
    data->temperatura_e=byte_a_decimal(&data->bits[16]);
    data->temperatura_d=byte_a_decimal(&data->bits[24]);
    data->checksum=byte_a_decimal(&data->bits[32]);
  
    printk(KERN_NOTICE "%d, %d, %d, %d, %d",data->humedad_e,data->humedad_d,data->temperatura_e,data->temperatura_d,data->checksum);
	
	
	/*Comprobamos que el checksum es correcto*/
	if(!(((data->humedad_e+data->humedad_d+data->temperatura_d+data->temperatura_e)&0xFF) == data->checksum)){
		 printk(KERN_NOTICE "Checksum no es correcto");
		 goto lectura_anterior;
	 }

	/*Preparamos los datos para ser asignados a la variable *val, que se encarga
	 * de almacenar el resultado a mostrar en la lectura del canal*/
	 
	if(data->temperatura_d!=0 || data->humedad_d!=0){ //Para el caso del DHT22
		 data->total_temp = (((data->temperatura_e & 0x7F) <<8) + data->temperatura_d) 
		                   * ((data->temperatura_e & 0x80) ? -100 : 100); //si el primer bit más significativo es 1, negativo
		                   
		 data->total_hum = ((data->humedad_e << 8)+data->humedad_d) * 100;
		 		 
	}else if(!data->temperatura_d && !data->humedad_d){ //para el caso del DHT11
		 data->total_temp = data->temperatura_e*1000;
		 data->total_hum = data->humedad_e*1000;
		 
	}else{
		 printk(KERN_NOTICE "ERROR al asignar datos a *val");
		 up(&critical_section);
		 return -EIO;
	}

	printk(KERN_NOTICE "data->total_temp = %d",data->total_temp);
	printk(KERN_NOTICE "data->total_hum = %d",data->total_hum);
	
lectura_anterior:   

	data->pos=0;
	data->time_end = ktime_get_boot_ns();

	if(channel->type == IIO_TEMP){
		*val = data->total_temp;  
	}else if(channel->type == IIO_HUMIDITYRELATIVE){
		*val = data->total_hum;
	}else if(channel->type == IIO_TIMESTAMP){
		*val = data->time_end - data->time_start;
	}else{
		up(&critical_section);
		return -EINVAL;
	}	
	printk(KERN_NOTICE "Time_start = %llu ns\n",data->time_start);
	printk(KERN_NOTICE "Time_end = %llu ns\n",data->time_end);
	printk(KERN_NOTICE "EXECUTION TIME = %llu ns\n",data->time_end - data->time_start);	
	up(&critical_section);
	return IIO_VAL_INT;
	
}


/* Función usada por el núcleo de IIO para leer los valores de los canales*/
static const struct iio_info IIO_info = {
		.driver_module = THIS_MODULE,
		.read_raw = read_raw,

};

/* Manejador de interrupción, se ejecuta en cada flanco de bajada y guarda un timestamp del
 * reloj de 16MHz de la raspberry para posteriormente poder medir el tiempo de cada trama de bits*/
static irqreturn_t irq_handler_DHT(int irq,void *dev){ 
	/*El argumento *dev es un puntero genérico al device registrado en request_irq(), en nuestro caso el iio_dev */
	struct iio_dev *iio = dev;
	struct private_data *data = iio_priv(iio);

	data->tiempos[data->pos]= ioread32(data->timer);
	(data->pos)++;
	/*Una vez que se complete la lectura de datos despertamos a la cola de espera */
	if(data->pos ==42){
		data->lock=0;
		wake_up_interruptible(&wait_queue);
	}
	
	return IRQ_HANDLED;
}

/*Función auxiliar para la configuración del GPIO e interrupción */
static int GPIO_config(struct iio_dev *iio, struct platform_device *pdev) {
    int res=0;
	struct private_data *data = iio_priv(iio);
	struct device *dev =&pdev->dev;
	struct device_node *np = dev->of_node; 
	
	data->gpio = of_get_gpio(np, 0);  /*Lectura del gpio desde device-tree */
	printk(KERN_NOTICE "GPIO read from device tree overlay is %d",data->gpio);
	if ((res = (data->gpio < 0))){
		printk(KERN_ERR "of_get_gpio function failure: %s\n", DHT_GPIO_DESC);
		return res;
	}
	
   if ((res=devm_gpio_request_one(dev, data->gpio,GPIOF_INIT_HIGH, DHT_GPIO_DESC))) { //solicitamos el gpio iniciado en nivel alto
        printk(KERN_ERR "GPIO request failure: %s\n", DHT_GPIO_DESC);
        return res;
    }
  
    printk(KERN_NOTICE "GPIO %d requested\n",data->gpio);
    
    
    if ((data->irq_DHT = gpio_to_irq(data->gpio)) < 0 ) { /*mapeamos gpio a irq */
        printk(KERN_ERR "GPIO to IRQ mapping failure %s\n", DHT_GPIO_DESC);

        return data->irq_DHT;
    }
    printk(KERN_NOTICE "Mapped irq %d to gpio %d\n", data->irq_DHT, data->gpio);
    
	return res;
}

/*Función que se ejecuta a la instalación del módulo que registra y reserva memoria
 * para el dispositivo */
static int DHT11_Probe(struct platform_device *pdev){
	int res=0;
	struct device *dev = &pdev->dev;
	struct iio_dev *iio;
	struct private_data *data;
	
	printk(KERN_NOTICE "Hello, loading %s module!\n", KBUILD_MODNAME);
	/*Reservamos memoria para el dispositivo */
	iio = devm_iio_device_alloc(dev, sizeof(*data));
	
	if(!iio){
		dev_err(dev, "Failed to allocate device\n");
		return -ENOMEM;
	}
	data = iio_priv(iio);  /*data apunta a la estructura privada del device iio */
	data->dev = dev;
	data->irq_DHT = 0; 

	printk(KERN_NOTICE "%s - GPIO config...\n", KBUILD_MODNAME);

	if((res = GPIO_config(iio,pdev))) {
		// Si al pedir y reservar los gpios hay algún problema se llamará a la función remove y se acabará el programa
		dev_err(&pdev->dev, "Failed to config GPIO\n");
	    DHT11_Remove(pdev);  
		return res;
	}
	
	data->timer = ioremap(PI2_TIMER_PAGE_BASE+ TIMER_OFFSET, SZ_4K); /*Mapeamos el timer con su dirección, offset y tamaño*/

		/*Inicializamos todas las variables de nuestra estructura para la primera lectura*/
	
	data->humedad_e=0;
	data->humedad_d=0;
	data->temperatura_d=0;
	data->temperatura_e=0;
	data->total_hum=0;
	data->total_temp=0;
	data->checksum=0;
	data->pos=0;
	data->lock=0;

		/*Definimos los campos del device iio con sus respectivos parámetros */

	iio->dev.parent = &pdev->dev;  
	iio->info = &IIO_info;   
	iio->name = KBUILD_MODNAME;
	iio->modes = INDIO_DIRECT_MODE;
	iio->channels = dht11_channels; 
	iio->num_channels = ARRAY_SIZE(dht11_channels);

	platform_set_drvdata(pdev,iio); /* iio_dev disponible para función remove*/

	res = devm_iio_device_register(&pdev->dev,iio);
	if(res){
		DHT11_Remove(pdev);
	    return res;
	}
	
	printk(KERN_NOTICE "%s module, Probe function performed\n", KBUILD_MODNAME);
	return res;

}

/* Estructura donde se determina la función a llamar en cada caso cuando el campo
 * .of_match_table ha sido pareado por el driver */
static struct platform_driver mypdrv ={
		.probe = DHT11_Probe,
		.remove = DHT11_Remove,
		.driver = {
				.name = "dht11_malaga",
				.of_match_table = iio_dht11_ids,
				.owner = THIS_MODULE,
		},
};
/*Esta macro registrará y desregistrará el platform_driver por nosotros, reemplaza a 
 * module_init() y module_exit() */
module_platform_driver(mypdrv);

MODULE_AUTHOR("Francisco Javier Moreno Conde <fran_10_cam@hotmail.com>");
MODULE_LICENSE("GPL");
