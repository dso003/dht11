#!/bin/bash
path=/home/pi/test
while true
do
TIME=`cat /sys/bus/iio/devices/iio\:device0/in_timestamp_input`
echo "Execution Time = $TIME ns"
TEMP=`cat /sys/bus/iio/devices/iio\:device0/in_temp_input`
TEMP_F=`echo "$TEMP 1000" | awk '{printf "%.2f \n", $1/$2}'`
echo "La temperatura es de $TEMP_F ºC"
HUM=`cat /sys/bus/iio/devices/iio\:device0/in_humidityrelative_input`
HUM_F=`echo "$HUM 1000" | awk '{printf "%.2f \n", $1/$2}'`
echo "La humedad relativa del ambiente es del $HUM_F %"
if [ -f "$path" ]
then
	echo "$TIME $HUM_F $TEMP_F" >> "$path"
fi
sleep 3
clear 
done
